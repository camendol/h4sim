#ifndef modulelecture_h
#define modulelecture_h 1


#include "globals.hh"

class lecturexml
{
   public:
   
     lecturexml(char*);
     ~lecturexml();
     
   public:
     
     void fichiercristal(FILE *ptrGeometrie);
     void fichierorientation(FILE *ptrOrientation);
     void fichierRotation(char *rot);
     void fichierAlveole(FILE *ptrOrientation);
     void rechRotation(char *mot,FILE *tempo,char* texte);
     double unites(double valeur,FILE *tempo);
     void recherche(char *mot,FILE *tempo, int sortie,char *texte);
     double recherche( char *mot,FILE *tempo, int sortie);
     std::string xnom();

     double parm0();
     double parm1();
     double parm2();
     double parm3();
     double parm4();
     double parm5();
     double parm6();
     double parm7();
     double parm8();
     double parm9();
     double parm10();
     double translate0();
     double translate1();
     double translate2();
     double translateAl0();
     double translateAl1();
     double translateAl2();
     double angles0();
     double angles1();
     double angles2();
     double angles3();
     double angles4();
     double angles5();
   
   private:
   
  char geometrypath[150];
     int compteur;
     char car[64];
     char passage[64];
     char nom_cristal[64];
     
     
     FILE *ptrOrientation;
     FILE *ptrOrientation2;
     FILE *ptrRot;
     
     double parametre[11];
     char cherche[64];
     int nb_caractere;
     double translate[3];
     double translateAl[3];
     double angles[6];
     char rotate[64];
     char rotateAl[64];
};


     
     




#endif
