
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include "moduleDetectorMessenger.hh"

#include "moduleDetectorConstruction.hh"
#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWithADouble.hh"
#include "G4UIcmdWithoutParameter.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

moduleDetectorMessenger::moduleDetectorMessenger(
                                           moduleDetectorConstruction* moduleDet)
:moduleDetector(moduleDet)
{ 
  moduledetDir = new G4UIdirectory("/calor/");
  moduledetDir->SetGuidance("module detector control.");
      
  Vis_SM_Cmd = new G4UIcmdWithAnInteger("/calor/module",this);
  Vis_SM_Cmd->SetGuidance("Choisir le module : 0000 0001 0010 0100 1000 1111");
  Vis_SM_Cmd->SetParameterName("choice",false);
  Vis_SM_Cmd->SetDefaultValue(1111);
  Vis_SM_Cmd->AvailableForStates(G4State_Idle);
  
  Vis_Alu_Cmd = new G4UIcmdWithAString("/calor/Alu",this);
  Vis_Alu_Cmd->SetGuidance("Plaques d'aluminium.");
  Vis_Alu_Cmd->SetParameterName("choice",false);
  Vis_Alu_Cmd->SetCandidates("on off");
  Vis_Alu_Cmd->AvailableForStates(G4State_Idle);
  
  Set_PosXY_Cmd = new G4UIcmdWithAString("/calor/SetPosXY",this);
  Set_PosXY_Cmd->SetGuidance("Move TB table along X and Y direction");
  Set_PosXY_Cmd->SetParameterName("choice",false);
  Set_PosXY_Cmd->SetDefaultValue("0. 0. cm");
  Set_PosXY_Cmd->AvailableForStates(G4State_Idle);
  
  Set_ModAngles_Cmd = new G4UIcmdWithAString("/calor/SetModAngles",this);
  Set_ModAngles_Cmd->SetGuidance("Rotate Module in Theta and Phi (around Y and Z direction");
  Set_ModAngles_Cmd->SetParameterName("choice",false);
  Set_ModAngles_Cmd->SetDefaultValue("0. 0. deg");
  Set_ModAngles_Cmd->AvailableForStates(G4State_Idle);
  
  UpdateCmd = new G4UIcmdWithoutParameter("/calor/update",this);
  UpdateCmd->SetGuidance("Update calorimeter geometry.");
  UpdateCmd->SetGuidance("This command MUST be applied before \"beamOn\" ");
  UpdateCmd->SetGuidance("if you changed geometrical value(s).");
  UpdateCmd->AvailableForStates(G4State_Idle);
      
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

moduleDetectorMessenger::~moduleDetectorMessenger()
{
  delete Vis_SM_Cmd;
  delete Vis_Alu_Cmd;
  delete Set_PosXY_Cmd;
  delete Set_ModAngles_Cmd;
  delete UpdateCmd;
  delete moduledetDir;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void moduleDetectorMessenger::SetNewValue(G4UIcommand* command,G4String newValue)
{ 
  if( command == Vis_SM_Cmd )
   { moduleDetector->SetVisSM(Vis_SM_Cmd->GetNewIntValue(newValue));}
   
  if( command == Vis_Alu_Cmd )
   { moduleDetector->SetVisAlu(newValue);}
  
  if( command == Set_PosXY_Cmd )
   { moduleDetector->SetPosXY(newValue);}
  
  if( command == Set_ModAngles_Cmd )
   { moduleDetector->SetModAngles(newValue);}
  
  if( command == UpdateCmd )
   { moduleDetector->UpdateGeometry(); }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
