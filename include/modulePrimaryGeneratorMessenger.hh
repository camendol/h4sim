
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#ifndef modulePrimaryGeneratorMessenger_h
#define modulePrimaryGeneratorMessenger_h 1

#include "G4UImessenger.hh"
#include "globals.hh"

class modulePrimaryGeneratorAction;
class G4UIcmdWithAString;
class G4UIcmdWithADouble;
class G4UIcmdWithAnInteger;
class G4UIcmdWithADoubleAndUnit;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class modulePrimaryGeneratorMessenger: public G4UImessenger
{
  public:
    modulePrimaryGeneratorMessenger(modulePrimaryGeneratorAction*);
   ~modulePrimaryGeneratorMessenger();
    
    void SetNewValue(G4UIcommand*, G4String);
    
  private:
  modulePrimaryGeneratorAction* moduleAction; 
  G4UIcmdWithAString*            beamtype;
  G4UIcmdWithAString*            beamprofile;
  G4UIcmdWithAnInteger*          posEta;
  G4UIcmdWithAnInteger*          posPhi;

  G4UIcmdWithADoubleAndUnit * beamxmin;
  G4UIcmdWithADoubleAndUnit * beamxmax;
  G4UIcmdWithADoubleAndUnit * beamxmean;
  G4UIcmdWithADoubleAndUnit * beamxsigma;
  G4UIcmdWithADoubleAndUnit * beamymin;
  G4UIcmdWithADoubleAndUnit * beamymax;
  G4UIcmdWithADoubleAndUnit * beamymean;
  G4UIcmdWithADoubleAndUnit * beamysigma;

  G4UIcmdWithADoubleAndUnit * xoff;
  G4UIcmdWithADoubleAndUnit * yoff;
  G4UIcmdWithADouble* dtheta;
  G4UIcmdWithADouble* dphi;
  G4UIcmdWithADoubleAndUnit * dx;
  G4UIcmdWithADoubleAndUnit * dy; 
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif

